$("#silencio").click(function(){

	if($(this).hasClass('glyphicon-volume-off')){
		$(this).removeClass('glyphicon-volume-off').addClass('glyphicon-volume-up');
		document.getElementById("audio").volume = 0.0;
	}else{
		$(this).addClass('glyphicon-volume-off').removeClass('glyphicon-volume-up');
		document.getElementById("audio").volume = 1.0;
	}	
});