var num_opciones = 2;
var numcheckbox = 0;
function habilitaDeshabilita(esto){
    if (esto.checked){
        if (numcheckbox >= num_opciones) {
                esto.checked = false;
                alert ("Solo puedes elegir dos opciones");
        } else {
                numcheckbox++;
        }
    } else {
        numcheckbox--;
    }
}

$(document).on("ready",function(){
	
	function radio(respuesta,numero,ultimo) {
		var ultimo = (ultimo!=0)? 'ultimo':'';
		if(respuesta!=null && respuesta!=""){
			return '<div class="radio"><label><input type="radio" class="respuestas radios '+ultimo+'" name="respuestas" value="'+numero+'" >'+respuesta+'</label></div>';
		}
	}
	function checkbox(respuesta,limite,numero,ultimo) {
		var limite = (limite!=0)? 'onchange="habilitaDeshabilita(this)"':'';
		var ultimo = (ultimo!=0)? 'ultimo':'';
		if(respuesta!=null && respuesta!=""){
			return '<div class="checkbox"><label><input type="checkbox" class="respuestas checkboxs '+ultimo+'" value="'+numero+'" '+limite+'>'+respuesta+'</label></div>';
		}
	}
	$(document).on("click",".respuestas:checked",function(){
		if($(this).hasClass('ultimo')){
			var otros = $("#otros").css("display");
			if(otros=="block"){
				$("#otros").css("display","none");	
				
			}else{
				$("#otros").css("display","block");
			}
		}

	});
	
	$(".listo").click(function(){

		var valor = $("textarea").val();
		if(valor!=""){
			$.ajax({
				type:"POST",
				data:{listo:"1",respuesta: valor}
			}).done(function(data){
				console.log(data);
				window.location=base_url+"inicio";
			})
			.fail(function(data) {
		    	console.log("error");
		    	console.log(data.responseText);
		    })
		    .always(function(data) {
		    	console.log("complete");
		    	console.log(data);
		    });
				
		}else{
			alert("Tienes que llenar el campo");
		}
	});

	$(".iniciar").click(function(){
		$.ajax({
			type: 'GET',
		})
		.done(function(data) {
			console.log(data);
			$(".respuestas").text("");
			$(".pregunta").text("");

			$("#numero").text(data.ID+" de 12");
			$(".pregunta").html(data.ID+".- "+data.Pregunta);
			var arreglo = [data.R6,data.R5,data.R4,data.R3,data.R2,data.R1];
			switch(data.Tipo){
				case "1":
					var ultimo = 1;
					var numero = 0;
					$.each(arreglo, function(index, val) {
						if(val!=""){
						 	numero++;
						 	$(".respuestas").prepend(radio(val,numero,ultimo));
						 	ultimo = 0;
						 }
					});
				break;
				case "2":
					
					var limite = (data.ID==1)?"1":"0";
					var ultimo = 1;
					var numero = 0;
					
					$.each(arreglo, function(index, val) {
						if(val!=""){
						 	numero++;
						 	
						 	$(".respuestas").prepend(checkbox(val,limite,numero,ultimo));
						 	ultimo = 0;
						 }
					});
				break;
				case "3":
					$(".respuestas").prepend("<div class='form-group'><div class='col-sm-12'><textarea class='form-control'></textarea></div></div>");
				break;
			}
			$(".iniciar").css("display","none");
			$(".siguiente").css('display', 'block');
		})
		.fail(function(data) {
			console.log(data);
		});
	});
	$(".siguiente").click(function(event) {
		var checkboxs = $(".respuestas:checked").hasClass('checkboxs');
		var radios = $(".respuestas:checked").hasClass('radios');
		
		console.log(checkboxs);
		console.log(radios);
		if(checkboxs==true||radios==true){
			var valor;
			if(checkboxs==true){
				valor = new Array();
	 			$(".checkboxs:checked").each(function() {
		            valor.push($(this).val());
		        });
			}

			if(radios==true){
				valor = $(".radios:checked").val();
			}
			if(typeof(valor) == "object"){
				var uno = null;
				$.each(valor, function(index, val) {
					if(val==1){
						uno = index;
					}
				});
				valor[uno] = $("#otro").val();
			}else{
				if(valor==1){
					valor = $("#otro").val();
				}
			}
			console.log(valor);
			$(".siguiente").attr('disabled', 'disabled');
			$.ajax({
				type: 'POST',
				data: {respuesta: valor},
			}).done(function(data) {
				console.log(data);
				$("#otros").css("display","none");
				$("#otro").val("");
				$.ajax({
					type: 'GET',
				})
				.done(function(data) {
					console.log("success");
					console.log(data);
					if(typeof(data.ultimo)!="undefined"){
						$(".listo").css("display","block");
						$(".siguiente").css('display', 'none');

					}
					$(".siguiente").removeAttr('disabled');
					$(".respuestas").text("");
					$(".pregunta").text("");

					$("#numero").text(data.ID+" de 12");
					$(".pregunta").html(data.ID+".- "+data.Pregunta);
					var arreglo = [data.R6,data.R5,data.R4,data.R3,data.R2,data.R1];
					switch(data.Tipo){
						case "1":
							var ultimo = 1;
							var numero = 0;
							$.each(arreglo, function(index, val) {
								if(val!=""){
								 	numero++;
								 	$(".respuestas").prepend(radio(val,numero,ultimo));
								 	ultimo = 0;
								 }
							});
						break;
						case "2":
							
							var limite = (data.ID==1)?"1":"0";
							var ultimo = 1;
							var numero = 0;
							
							$.each(arreglo, function(index, val) {
								if(val!=""){
								 	numero++;
								 	
								 	$(".respuestas").prepend(checkbox(val,limite,numero,ultimo));
								 	ultimo = 0;
								 }
							});
						break;
						case "3":
							$(".respuestas").prepend("<div class='form-group'><div class='col-sm-12'><textarea class='form-control'></textarea></div></div>");
						break;
					}
				})
				.fail(function(data) {
					console.log(data);
					$("#result").text(data);
				});
			}).fail(function(data) {
				console.log(data);
				$("#result").text(data);
			});

		}else{
			alert("Tiene que responder antes de avanzar a la siguiente pregunta");
		}
	});
	
});