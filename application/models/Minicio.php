<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Minicio extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('date');
		$this->load->database();
	}

	public function registro_usuario($data)
	{
		if($this->db->insert('donadores', $data)){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	public function inicio_usuario($data)
	{
		$this->db->where('Correo', $data["Correo"]);
		$this->db->where('Password', $data["Password"]);
		$query = $this->db->get('donadores');
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}

	public function getDatos_byCorreo($data)
	{
		$this->db->where('Correo', $data["Correo"]);
		$query = $this->db->get('donadores');
		if($query->num_rows() > 0){
			return $query->result_array();;
		}else{
			return FALSE;
		}
	}
	
	
	public function setToken_byId($datos)
	{
		$this->db->insert('tokens', $datos);
	}
	public function setPassword($id,$password)
	{
		$data = array("Password" => $password);
		$this->db->where('ID', $id);
		$this->db->update('donadores', $data); 
	}

	public function registro_datospersonales($data)
	{
		if($this->db->insert('datos_donadores', $data)){
			return true;
		}else{
			return false;
		}
	}

	public function getPregunta($id)
	{
		$this->db->where('ID', $id);
		$query = $this->db->get('preguntas');
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getNinos()
	{
		$query = $this->db->get('ninos');
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getComida()
	{
		$query = $this->db->get('comida');
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getPassword($correo)
	{
		$this->db->select('Correo');
		$query = $this->db->get_where('donadores', array('Correo' => $correo));
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return FALSE;
		}	
	}
	public function setEncuesta($data)
	{

		$this->db->insert('respuestas', $data);
	}
	public function getEncuesta($id)
	{
		$this->db->where('ID', $id);
		$query = $this->db->get('respuestas');
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getBoolean_byCorreo($correo)
	{
		$this->db->where('Correo', $correo);
		$query = $this->db->get('donadores');
		if($query->num_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function setRegalo($data)
	{
		$this->db->insert('donacion_nino', $data);
	}
	public function getListaNinos()
	{
		$query = $this->db->query("SELECT * FROM donacion_nino INNER JOIN donadores ON donacion_nino.ID_Donador = donadores.ID");
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getListaComida()
	{
		$query =  $this->db->query("SELECT * FROM donacion_comida INNER JOIN donadores ON donacion_comida.ID_Donador = donadores.ID");
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getDatos_ByItem($id)
	{
		$this->db->where('ID', $id);
		$query = $this->db->get('comida');
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getBoolean_byItem($id)
	{
		$this->db->where('Item', $id);
		$query = $this->db->get('donacion_comida');
		if($query->num_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function setComida($datos)
	{
		$this->db->insert('donacion_comida', $datos);
	}
	public function setAsistencia($data)
	{
		$this->db->insert('asistencia', $data);
	}
	public function updateAsistencia($id,$data)
	{
		$this->db->where('ID_Donador',$id);
		$this->db->update('asistencia', $data);
	}
	public function getID_ByToken($token)
	{
		$this->db->where('Token', $token);
		$this->db->where('Habilitado', "1");
		$query = $this->db->get('tokens');
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getBoolean_byToken($token){
		$this->db->where('Token', $token);
		$this->db->where('Habilitado', "1");
		$query = $this->db->get('tokens');
		if($query->num_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function updateToken($token)
	{

		$data  = array('Habilitado' => "0");
		$this->db->where('Token', $token);
		$this->db->update('tokens', $data);
	}
	public function getAsistentes($id)
	{
		$this->db->where('ID_Donador', $id);
		$query = $this->db->get('asistencia');
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	
	public function getDatos_byID($id)
	{
		$this->db->where('ID', $id);
		$query = $this->db->get('donadores');
		if($query->num_rows() > 0){
			return $query->result_array();;
		}else{
			return FALSE;
		}
	}
	public function getTablaTokens()
	{
		$query = $this->db->get('tokens');
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	
	public function getTablaNinos()
	{
		$query = $this->db->query("SELECT * FROM donacion_nino INNER JOIN donadores ON donacion_nino.ID_Donador=donadores.ID ORDER BY donadores.Correo");	
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getTablaComida()
	{
		$query = $this->db->query("SELECT * FROM donacion_comida INNER JOIN donadores ON donacion_comida.ID_Donador=donadores.ID ORDER BY donadores.Correo");
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
}	

/* End of file minicio.php */
/* Location: ./application/models/minicio.php */