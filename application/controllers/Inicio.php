<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inicio extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->library('encrypt');

		$this->load->library('session');
		$this->load->library('email');
		$this->load->model('minicio');
	}

	public function index()
	{
		if($this->session->userdata('usuario')){
			$data['titulo'] = "Casa Hogar Vallado A.C.";
			$data['css'] = array('bootstrap.min');
			$data['script'] = array('jquery', 'bootstrap');
			$data['menu'] = $this->load->view('menu', "", TRUE);
			$data['contenido'] = $this->load->view('inicio/dashboard', "", TRUE);
			$this->load->view('html', $data, FALSE);
		}else{
			$data['titulo'] = "Casa Hogar Vallado A.C.";
			$data['css'] = array('bootstrap.min');
			$data['script'] = array('jquery', 'bootstrap');
			$data['menu'] = $this->load->view('menu', "", TRUE);
			$data['contenido'] = $this->load->view('inicio/index', "", TRUE);
			$this->load->view('html', $data, FALSE);
		}
	}
	public function datos()
	{
		if(!$this->session->userdata('usuario')){
			redirect('login','refresh');
		}
		if($this->input->post()==NULL){
			$data['titulo'] = "Casa Hogar Vallado A.C.";
			$data['css'] = array('bootstrap.min');
			$data['script'] = array('jquery', 'bootstrap','datos');
			$data['menu'] = $this->load->view('menu', "", TRUE);
			$data['contenido'] = $this->load->view('inicio/registrosecond', "", TRUE);
			$this->load->view('html', $data, FALSE);
		}else{
			$this->load->model('minicio');
			$data = $this->input->post();

			$this->form_validation->set_rules('Edad', 'Edad', 'trim|required|integer');
			$this->form_validation->set_rules('Genero', 'Genero', 'trim|required|integer');
			$this->form_validation->set_rules('Ocupacion', 'Ocupacion', 'trim|required');
			$this->form_validation->set_rules('EstadoCivil', 'Estado Civil', 'trim|required');
			$this->form_validation->set_rules('Estudios', 'Nivel Estudios', 'trim|required');
			$this->form_validation->set_rules('Hogar', 'Integrantes en el hogar', 'trim|required|integer');
			$this->form_validation->set_rules('Pasatiempo', 'Pasatiempo', 'trim|required');
			$this->form_validation->set_rules('Contacto', 'Contacto', 'trim|required');

			foreach ($data as $key => $value) {
				$data["$key"] = $this->security->xss_clean($value);
			}

			if ($this->form_validation->run() == FALSE) {
			   	$datos['titulo'] = "Casa Hogar Vallado A.C.";
				$datos['css'] = array('bootstrap.min');
				$datos['script'] = array('jquery', 'bootstrap','datos');
				$datos['menu'] = $this->load->view('menu', "", TRUE);
				$datos['contenido'] = $this->load->view('inicio/registrosecond', $data, TRUE);
				$this->load->view('html', $datos, FALSE);
			}else{
				if($data["Ocupacion"]==5){
					$data["Ocupacion"] = $data["Ocupacion1"];
				}
				if($data["EstadoCivil"]==4){
					$data["EstadoCivil"] = $data["EstadoCivil1"];
				}
				if($data["Estudios"]==4){
					$data["Estudios"] = $data["Estudios1"];
				}
				if($data["Contacto"]==5){
					$data["Contacto"] = $data["Contacto1"];
				}
				unset($data["Ocupacion1"]);
				unset($data["EstadoCivil1"]);
				unset($data["Estudios1"]);
				unset($data["Contacto1"]);

				$data["ID_Donador"] = $this->encrypt->decode($this->session->userdata('usuario'));
				if ($this->minicio->registro_datospersonales($data)){
					redirect('inicio','refresh');
				}
				else{
					$datos['titulo'] = "Casa Hogar Vallado A.C.";
					$datos['css'] = array('bootstrap.min');
					$datos['script'] = array('jquery', 'bootstrap','datos');
					$datos['menu'] = $this->load->view('menu', "", TRUE);
					$datos['contenido'] = $this->load->view('inicio/registrosecond', $data, TRUE);
					$this->load->view('html', $datos, FALSE);
				}
			}

		}
	}

	public function registro()
	{
		if($this->session->userdata('usuario')){
			redirect('inicio','refresh');
		}
		if($this->input->post()==NULL){
			$data['titulo'] = "Casa Hogar Vallado A.C.";
			$data['css'] = array('bootstrap.min');
			$data['script'] = array('jquery', 'bootstrap');
			$data['menu'] = $this->load->view('menu', "", TRUE);
			$data['contenido'] = $this->load->view('inicio/registro', "", TRUE);
			$this->load->view('html', $data, FALSE);
		}else{
			$this->load->model('minicio');
			$data = $this->input->post();
			$datosViejos = $data;
			$this->form_validation->set_rules('Nombre', 'Nombre', 'trim|required|min_length[1]');
			$this->form_validation->set_rules('ApellidoP', 'Apellido Paterno', 'trim|required|min_length[1]');
			$this->form_validation->set_rules('ApellidoM', 'Apellido Materno', 'trim|required|min_length[1]');
			$this->form_validation->set_rules('Correo', 'Correo electrónico', 'trim|required|valid_email');
			$this->form_validation->set_rules('Password', 'Contraseña', 'trim|required|min_length[8]');

			foreach ($data as $key => $value) {
				if($key == "Password" || $key == "Correo"){
					continue;
				}
				$data["$key"] = $this->security->xss_clean($value);
			}
			$data["Password"] = sha1(md5($data["Password"]."toroloco")); 
			$true= $this->minicio->getBoolean_byCorreo($data["Correo"]);
			
			if ($this->form_validation->run() == FALSE||$true==TRUE) {
					if($true==TRUE){
						$dataViejos["error"] = "Este correo electrónico se encuentra en uso.";
					}
				   	$datos['titulo'] = "Casa Hogar Vallado A.C.";
					$datos['css'] = array('bootstrap.min');
					$datos['script'] = array('jquery', 'bootstrap');
					$datos['menu'] = $this->load->view('menu', "", TRUE);
					$datos['contenido'] = $this->load->view('inicio/registro', $datosViejos, TRUE);
					$this->load->view('html', $datos, FALSE);
			}else {

				$data["FechaRegistro"] = date('Y-m-d H:i:s');
				$data["FechaUltimoEntrada"] = date('Y-m-d H:i:s');
				$id = $this->minicio->registro_usuario($data);
				if($id!=FALSE){
					$array = array(
						'usuario' => $this->encrypt->encode($id),
					);
					
					$this->session->set_userdata( $array );
				}
				if ($id!=FALSE){
					$this->load->library('email');
					$email_setting  = array('mailtype'=>'html');
					$this->email->initialize($email_setting);
					$this->email->from('cristina.palos@upslp.edu.mx', 'Casa Hogar Vallado');
					$this->email->to($datosViejos["Correo"]);
					
					$this->email->subject('Gracias por registrarte en Casa Hogar Vallado A.C.');
					$email["nombre"] = $datosViejos["Nombre"].' '.$datosViejos["ApellidoP"].' '.$datosViejos["ApellidoM"];
					$mensaje = $this->load->view('inicio/mensaje',$email,TRUE);
					$this->email->message($mensaje);
					$this->email->send();

					redirect('datos','refresh');
				}else{
					$datosViejos["error"] = "Correo electrónico y/o Contraseña incorrectos";
					$datos['titulo'] = "Casa Hogar Vallado A.C.";
					$datos['css'] = array('bootstrap.min');
					$datos['script'] = array('jquery', 'bootstrap');
					$datos['menu'] = $this->load->view('menu', "", TRUE);
					$datos['contenido'] = $this->load->view('inicio/registro', $datosViejos, TRUE);
					$this->load->view('html', $datos, FALSE);
				}
				
			}
			
		}

	}
	public function salir()
	{
		$this->session->unset_userdata('usuario');
		redirect('inicio','refresh');
	}
	public function login()
	{
		if($this->session->userdata('usuario')){
			redirect('inicio','refresh');
		}
		$data = $this->input->post();
		if($data==NULL){
			$data['titulo'] = "Casa Hogar Vallado A.C.";
			$data['css'] = array('bootstrap.min');
			$data['script'] = array('jquery', 'bootstrap');
			$data['menu'] = $this->load->view('menu', "", TRUE);
			$data['contenido'] = $this->load->view('inicio/login', "", TRUE);
			$this->load->view('html', $data, FALSE);
		}else{
			$this->load->model('minicio');
			$data = $this->input->post();
			$datosViejos = $data;
			$this->form_validation->set_rules('Correo', 'Correo electrónico', 'trim|required|valid_email');
			$this->form_validation->set_rules('Password', 'Contraseña', 'trim|required');
			$data["Password"] = sha1(md5($data["Password"]."toroloco"));

			if ($this->form_validation->run() == FALSE) {
			   	$datos['titulo'] = "Casa Hogar Vallado A.C.";
				$datos['css'] = array('bootstrap.min');
				$datos['script'] = array('jquery', 'bootstrap');
				$datos['menu'] = $this->load->view('menu', "", TRUE);
				$datos['contenido'] = $this->load->view('inicio/login', $datosViejos, TRUE);
				$this->load->view('html', $datos, FALSE);
			}else {
				$id = $this->minicio->inicio_usuario($data);
				if($id!=FALSE){
					$id = $id["0"]["ID"];
					$array = array(
						'usuario' => $this->encrypt->encode($id),
					);
					
					$this->session->set_userdata( $array );
				}
				if ($id!=FALSE){
					if($this->session->userdata('variableVerga')){
						redirect('inicio/confirmarAsistencia','refresh');
					}else{
						redirect('inicio','refresh');	
					}
				}else{
					$datosViejos["error"] = "Correo electrónico y/o contraseña incorrectos";
					$datos['titulo'] = "Casa Hogar Vallado A.C.";
					$datos['css'] = array('bootstrap.min');
					$datos['script'] = array('jquery', 'bootstrap');
					$datos['menu'] = $this->load->view('menu', "", TRUE);
					$datos['contenido'] = $this->load->view('inicio/login', $datosViejos, TRUE);
					$this->load->view('html', $datos, FALSE);
				}
				
			}
		}
	}

	public function encuesta()
	{
		if(!$this->session->userdata('usuario')){
			redirect('inicio','refresh');
		}
		if($this->minicio->getEncuesta($this->encrypt->decode($this->session->userdata('usuario')))){
			redirect('inicio','refresh');
		}
		if($this->input->is_ajax_request()){
			$data = $this->input->post();
			if($data!=NULL){
				//POST
				$data["id"] = $this->session->userdata('encuesta');
				$data["respuesta"] = $this->input->post('respuesta');
				$data["listo"] = $this->input->post('listo', TRUE);
				if($data["respuesta"]!=NULL){
					$data["array"] = NULL;
					if(is_array($data["respuesta"])){
						foreach ($data["respuesta"] as $key => $value) {
							if($key==0){
								$data["array"] = array(
									'A'.$data["id"] => $value
								);
							}else{	
								$data["array"] = array(
									'A'.$data["id"]."-".$key => $value
								);
							}
							$this->session->set_userdata( $data["array"] );
						}
					}else{
						$data["array"] = array(
							'A'.$data["id"] => $data["respuesta"]
						);
						$this->session->set_userdata( $data["array"] );
					}
					do{
						$encuesta = array(
							'encuesta' => $data["id"]+1
						);
						$this->session->set_userdata( $encuesta );
					}while($data["id"] == $this->session->userdata('encuesta'));

					$this->output->set_content_type('application/json')->set_output(json_encode($data));
				}
				if ($data["listo"]!=NULL) {
					$datos["ID"] = $this->encrypt->decode($this->session->userdata('usuario'));
					$datos["A1"] = $this->session->userdata('A1');
					$datos["A1-1"] = $this->session->userdata('A1-1');
					$datos["A2"] = $this->session->userdata('A2');
					$datos["A3"] = $this->session->userdata('A3');
					$datos["A4"] = $this->session->userdata('A4');
					$datos["A5"] = $this->session->userdata('A5');
					$datos["A6"] = $this->session->userdata('A6');
					$datos["A7"] = $this->session->userdata('A7');
					$datos["A8"] = $this->session->userdata('A8');
					$datos["A9"] = $this->session->userdata('A9');
					$datos["A10"] = $this->session->userdata('A10');
					$datos["A10-1"] = $this->session->userdata('A10-1');
					$datos["A11"] = $this->session->userdata('A11');
					$datos["A12"] = $this->session->userdata('A12');
					$datos["Fecha"] = date('Y-m-d H:i:s');
					$this->minicio->setEncuesta($datos);
					$this->output->set_content_type('application/json')->set_output(json_encode($datos));
				}
			}else{
				//GET
				$data["id"] =$this->session->userdata('encuesta');
				//$data["id"] = $this->input->get('id');
				$datos = $this->minicio->getPregunta($data["id"]);
				if($data["id"]==12){
					$datos["0"]["ultimo"] = 1;
				}
				$this->output->set_content_type('application/json')->set_output(json_encode($datos["0"]));
			}
		}else{
			$array = array(
				'encuesta' => 1
			);
			
			$this->session->set_userdata( $array );

			$data['titulo'] = "Casa Hogar Vallado A.C.";
			$data['css'] = array('bootstrap.min');
			$data['script'] = array('jquery', 'bootstrap',"encuesta");
			$data['menu'] = $this->load->view('menu', "", TRUE);
			$data['contenido'] = $this->load->view('inicio/encuesta', "", TRUE);
			$this->load->view('html', $data, FALSE);
		}
	}
	public function posada()
	{
		if(!$this->session->userdata('usuario')){
			redirect('inicio','refresh');
		}
		if($this->minicio->getEncuesta($this->encrypt->decode($this->session->userdata('usuario')))==FALSE){
			redirect('encuesta','refresh');
		}

		if($this->input->is_ajax_request()){
			$datas = $this->input->post();
			if($datas==NULL){
				//GET
				
			}else{
				//POST
				$data["regalo"] = $this->input->post('regalo', TRUE);

				if($data["regalo"]!=NULL){
					foreach ($data["regalo"] as $key => $value) {
						$data["ID_Donador"] = $this->encrypt->decode($this->session->userdata('usuario'));
						$data["ID_Nino"] = substr($value, 0,strpos($value, "-"));
						$data["Regalo"] = substr($value,strpos($value, "-")+1);
						$data["Fecha"] =  date('Y-m-d H:i:s');
						$data["Habilitado"] = 1;
						unset($data["regalo"]);
						$this->minicio->setRegalo($data);
					}
				}
				unset($data);
				$data["apoyo"] = $this->input->post('apoyo', TRUE);
				if($data["apoyo"]!=NULL){
					foreach ($data["apoyo"] as $key => $value) {
						if(is_numeric($value)){
							//Todo
							$dato = $this->minicio->getBoolean_byItem($value);
							if($dato==TRUE){
								//SI hay D:
								$data["Error"] = "Hubo un error con su aportación vuelva a llenar nuevamente los campos";
								$this->output->set_content_type('application/json')->set_output(json_encode($data));
							}else{
								$dataItem = $this->minicio->getDatos_ByItem($value);
								$datos["ID_Donador"] =  $this->encrypt->decode($this->session->userdata('usuario'));
								$datos["Item"] = $value;
								$datos["Cantidad"] = $dataItem["0"]["Costo"];
								$datos["Fecha"] =  date('Y-m-d H:i:s');
								$datos["Habilitado"] = 1;
								$this->minicio->setComida($datos);
							}
						}else{
							$datos["ID_Donador"] = $this->encrypt->decode($this->session->userdata('usuario'));
							$datos["Item"] = substr($value, 0,strpos($value, "-"));
							$datos["Cantidad"] = substr($value,strpos($value, "-")+1);
							$datos["Fecha"] =  date('Y-m-d H:i:s');
							$datos["Habilitado"] = 1;
							$this->minicio->setComida($datos);
						}
					}
				}
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
				//$this->output->set_content_type('application/json')->set_output(json_encode($data));
			}
		}else{
			$id = $this->encrypt->decode($this->session->userdata('usuario'));
			$datos["ninos"] = $this->minicio->getNinos();
			$datos["comida"] = $this->minicio->getComida();
			$datos["asistentes"] = $this->minicio->getAsistentes($id);
			
			$datos["asistentes"] = ($datos["asistentes"]==FALSE)? FALSE : $datos["asistentes"]["0"];

			$listaNinos = $this->minicio->getListaNinos();
			if(is_array($listaNinos)){
				foreach ($listaNinos as $key => $value) {
					$datos["listaNinos"][$value["ID_Nino"]."-".$value["Regalo"]] = $value["Nombre"]." ".$value["ApellidoP"]." ".$value["ApellidoM"];
				}
			}
			$listaComida = $this->minicio->getListaComida();
			if(is_array($listaComida)){
				foreach ($listaComida as $key => $value) {
					if(isset($datos["listaComida"][$value["Item"]]["Falta"])){
						$datos["listaComida"][$value["Item"]]["Falta"] += $value["Cantidad"];
					}else{
						$datos["listaComida"][$value["Item"]]["Falta"] = $value["Cantidad"];
					}
					if(isset($datos["listaComida"][$value["Item"]]["Padrinos"])){
						$datos["listaComida"][$value["Item"]]["Padrinos"] .= ",".$value["Nombre"]." ".$value["ApellidoP"]." ".$value["ApellidoM"];
					}else{
						$datos["listaComida"][$value["Item"]]["Padrinos"] = $value["Nombre"]." ".$value["ApellidoP"]." ".$value["ApellidoM"];
					}
				}
			}
			for ($i=1; $i <=18; $i++) { 
				$dataItem = $this->minicio->getDatos_ByItem($i);
				if(isset($datos["listaComida"][$i]["Falta"])){
					$datos["listaComida"][$i]["Falta"] = $dataItem["0"]["Costo"] - $datos["listaComida"][$i]["Falta"];
					$datos["listaComida"][$i]["Todo"] = 1;
				}else{
					$datos["listaComida"][$i]["Falta"] = $dataItem["0"]["Costo"];
				}
			}
			$data['titulo'] = "Casa Hogar Vallado A.C.";
			$data['css'] = array('bootstrap.min');
			$data['script'] = array('jquery', 'bootstrap',"posada");
			$data['menu'] = $this->load->view('menu', "", TRUE);
			$data['contenido'] = $this->load->view('inicio/posada', $datos, TRUE);
			$this->load->view('html', $data, FALSE);
		}
	}
	public function recuperar()
	{
		$this->load->model('minicio');

		if($this->input->post()==NULL){
			$data['titulo'] = "Casa Hogar Vallado A.C.";
			$data['css'] = array('bootstrap.min');
			$data['script'] = array('jquery', 'bootstrap');
			$data['menu'] = $this->load->view('menu', "", TRUE);
			$data['contenido'] = $this->load->view('inicio/recuperar', "", TRUE);
			$this->load->view('html', $data, FALSE);
			
	    }else{
	    	$data = $this->input->post();
			$this->form_validation->set_rules('Correo', 'Correo electrónico', 'trim|required|valid_email');
			if ($this->form_validation->run() == FALSE) {
				   	$datos['titulo'] = "Casa Hogar Vallado A.C.";
					$datos['css'] = array('bootstrap.min');
					$datos['script'] = array('jquery', 'bootstrap');
					$datos['menu'] = $this->load->view('menu', "", TRUE);
					$datos['contenido'] = $this->load->view('inicio/recuperar', "", TRUE);
					$this->load->view('html', $datos, FALSE);
			}else {
				$datos = $this->minicio->getDatos_byCorreo($data);
				if($datos!=FALSE){
					//Enviar Correo
					$datos = $datos["0"];
					$this->load->library('email');
					$email_setting  = array('mailtype'=>'html');
					$this->email->initialize($email_setting);
					$this->email->from('cristina.palos@upslp.edu.mx', 'Casa Hogar Vallado');
					$this->email->to($data["Correo"]);
					
					$this->email->subject('Recupera tu contraseña en Casa Hogar Vallado A.C.');
					$email["nombre"] = $datos["Nombre"].' '.$datos["ApellidoP"].' '.$datos["ApellidoM"];
					$email["token"] = sha1(md5($datos["ID"].date('Y-m-d H:i:s')."toroloco"));
					$ndatos["Token"] = $email["token"];
					$ndatos["ID_Donador"] = $datos["ID"];
					$ndatos["Fecha"] = date('Y-m-d H:i:s');
					$ndatos["Habilitado"] = 1;
					$this->minicio->setToken_byId($ndatos);
					$mensaje = $this->load->view('inicio/recuperarMail',$email,TRUE);
					$this->email->message($mensaje);
					$this->email->send();

					
					$datos["error"] = "Revisa tu bandeja de correo electrónico <a href='".base_url("login")."'>Inicar Sesión</a>";
				
				}else{
					$datos["error"] = "Este correo electrónico no esta registrado.";
				}
				$data['titulo'] = "Casa Hogar Vallado A.C.";
				$data['css'] = array('bootstrap.min');
				$data['script'] = array('jquery', 'bootstrap');
				$data['menu'] = $this->load->view('menu', "", TRUE);
				$data['contenido'] = $this->load->view('inicio/recuperar', $datos, TRUE);
				$this->load->view('html', $data, FALSE);

				
		    }
	    }
	}
	public function nuevoPassword()
	{
		
		if($this->input->post()==NULL){
			$data = $this->input->get();
			if(isset($data["token"])){
				if($this->minicio->getBoolean_byToken($data["token"])==TRUE){
					$datos['titulo'] = "Casa Hogar Vallado A.C.";
					$datos['css'] = array('bootstrap.min');
					$datos['script'] = array('jquery', 'bootstrap');
					$datos['menu'] = $this->load->view('menu', "", TRUE);
					$datos['contenido'] = $this->load->view('inicio/newPassword', $data, TRUE);
					$this->load->view('html', $datos, FALSE);
				}else{
					$datos['titulo'] = "Casa Hogar Vallado A.C.";
					$datos['css'] = array('bootstrap.min');
					$datos['script'] = array('jquery', 'bootstrap');
					$datos['menu'] = $this->load->view('menu', "", TRUE);
					$datos['contenido'] = $this->load->view('inicio/errorToken', "", TRUE);
					$this->load->view('html', $datos, FALSE);
				}
			}else{
				redirect('inicio','refresh');
			}
		}else{
			$data = $this->input->post();
			$this->form_validation->set_rules('Password', 'Contraseña', 'trim|required|min_length[8]');
			if ($this->form_validation->run() == FALSE) {
				$datos['titulo'] = "Casa Hogar Vallado A.C.";
				$datos['css'] = array('bootstrap.min');
				$datos['script'] = array('jquery', 'bootstrap');
				$datos['menu'] = $this->load->view('menu', "", TRUE);
				$datos['contenido'] = $this->load->view('inicio/newPassword', $data, TRUE);
				$this->load->view('html', $datos, FALSE);
			}else{
				
				$datos = $this->minicio->getID_ByToken($data["token"]);
				if($datos!=FALSE){
					$datos = $datos["0"];
					$password = sha1(md5($data["Password"]."toroloco"));
					$this->minicio->setPassword($datos["ID_Donador"],$password);
					$this->minicio->updateToken($data["token"]);
					redirect('login','refresh');
				}else{
					//Este token ya no sirve
					$datos['titulo'] = "Casa Hogar Vallado A.C.";
					$datos['css'] = array('bootstrap.min');
					$datos['script'] = array('jquery', 'bootstrap');
					$datos['menu'] = $this->load->view('menu', "", TRUE);
					$datos['contenido'] = $this->load->view('inicio/errorToken', "", TRUE);
					$this->load->view('html', $datos, FALSE);
				}
			}
			
		}
		
	}
	public function mapa()
	{
		if(!$this->session->userdata('usuario')){
			redirect('inicio','refresh');
		}
		if($this->minicio->getEncuesta($this->encrypt->decode($this->session->userdata('usuario')))==FALSE){
			redirect('encuesta','refresh');
		}
		$data['titulo'] = "Casa Hogar Vallado A.C.";
		$data['css'] = array('bootstrap.min');
		$data['script'] = array('jquery', 'bootstrap',"encuesta");
		$data['menu'] = $this->load->view('menu', "", TRUE);
		$data['contenido'] = $this->load->view('inicio/mapa', "", TRUE);
		$this->load->view('html', $data, FALSE);
	}
	public function error($value='')
	{
		$this->session->unset_userdata('A1-1');
	}

	public function gracias()
	{
		$data['titulo'] = "Casa Hogar Vallado A.C.";
		$data['css'] = array('bootstrap.min');
		$data['script'] = array('jquery', 'bootstrap',"gracias");
		$data['menu'] = $this->load->view('menu', "", TRUE);
		$data['contenido'] = $this->load->view('inicio/gracias', "", TRUE);
		$this->load->view('html', $data, FALSE);
	}
	public function mensaje()
	{
		$email["nombre"] = "pedro";
		$this->load->view('inicio/confirmar',$email);
	}
	public function confirmarAsistencia()
	{
		$this->session->set_userdata( array('variableVerga' => TRUE));
		if(!$this->session->userdata('usuario')){
			redirect('login','refresh');
		}
		$id = $this->encrypt->decode($this->session->userdata('usuario'));
		if($this->input->post()!=NULL){
			$data["Asistencia"] = $this->input->post('asistencia', TRUE);
			$datoss=null;
				
				
			if($data["Asistencia"]!=NULL){
				$i=0;
				foreach ($data["Asistencia"] as $key => $value) {
					if($key!="0"){
						$i++;
						$value = substr($value,strpos($value, "-")+1);
						$datoss["Edad".$i] = substr($value, 0,strpos($value, "-"));
						$datoss["Talla".$i] =substr($value,strpos($value, "-")+1);
					}
				}
				$datoss["Asistencia"] = ($data["Asistencia"]["0"]=="si")?1:0;
				$datoss["Fecha"] = date('Y-m-d H:i:s');
				$datoss["Habilitado"] = 1;
				if($this->minicio->getAsistentes($this->encrypt->decode($this->session->userdata('usuario')))==FALSE){
					$datoss["ID_Donador"] = $this->encrypt->decode($this->session->userdata('usuario'));
					$this->minicio->setAsistencia($datoss);
				}else{
					$id = $this->encrypt->decode($this->session->userdata('usuario'));
					$this->minicio->updateAsistencia($id,$datoss);
				}
				
			}
			
			//$this->output->set_content_type('application/json')->set_output(json_encode($datoss));
		}else{
			$datos["asistentes"] = $this->minicio->getAsistentes($id);
			$datos["asistentes"] = ($datos["asistentes"]==FALSE)? FALSE : $datos["asistentes"]["0"];
			$data['titulo'] = "Casa Hogar Vallado A.C.";
			$data['css'] = array('bootstrap.min');
			$data['script'] = array('jquery', 'bootstrap',"asistencia");
			$data['menu'] = $this->load->view('menu', "", TRUE);
			$data['contenido'] = $this->load->view('inicio/asistencia', $datos, TRUE);
			$this->load->view('html', $data, FALSE);
		}
		
	}
	
	public function errorContrasena()
	{
		/*
		echo "Niños<br/>";
		$anterior="";
		$datos = $this->minicio->getTablaNinos();
		foreach ($datos as $key => $value) {
			if($value["Correo"]!=$anterior){
				echo $value["Correo"]."<br/>";
			}	
			$anterior = $value["Correo"];
		}
		echo "Comida<br/>";
		$datos = $this->minicio->getTablaComida();
		foreach ($datos as $key => $value) {
			if($value["Correo"]!=$anterior){
				echo $value["Correo"]."<br/>";
			}	
			$anterior = $value["Correo"];
		}*/
		$correos["0"]="pedro_inat18@hotmail.com";
		$correos["1"]="120618@upslp.edu.mx";
		$correos["2"]="aleja_867@hotmail.com";
		$correos["3"]="ale_jltj@hotmail.com";
		$correos["4"]="analiliru18@hotmail.com";
		$correos["5"]="ara13_16@hotmail.com";
		$correos["6"]="beatriz_zamarripa_r@hotmail.com";
		$correos["7"]="belem098_7@hotmail.com";
		$correos["8"]="berenice.leyva@upslp.edu.mx";
		$correos["9"]="carla.cisneros92@gmail.com";
		$correos["10"]="casmar8788@gmail.com";
		$correos["11"]="deleon.francisco@remyinc.com";
		$correos["12"]="dinorah.cabrera@upslp.edu.mx";
		$correos["13"]="emmanuel.0531@gmail.com";
		$correos["14"]="javier.riverag21@gmail.com";
		$correos["15"]="lge_valdez@hotmail.com";
		$correos["16"]="meza_93_@hotmail.com";
		$correos["17"]="ma_meza@outlook.com";
		$correos["18"]="nava_21gp@hotmail.com";
		$correos["19"]="palomalag@hotmail.com";
		$correos["20"]="ricardo.ramosbarcenas@gmail.com";
		$correos["21"]="roxy_dl@hotmail.com";
		$correos["22"]="saira_kha@hotmail.com";
		$correos["23"]="soul_2001@hotmail.com";
		$correos["24"]="viktor.delgado.mtz@gmail.com";
		$correos["25"]="zaraith.ortega@gmail.com";
		$correos["26"]="pedroperafan18@gmail.com";
		for ($i=0; $i <=26 ; $i++) { 
			$this->load->library('email');
			$this->email->clear();
			$email_setting  = array('mailtype'=>'html');
			$this->email->initialize($email_setting);
			$this->email->from('cristina.palos@upslp.edu.mx', 'Casa Hogar Vallado');
			$this->email->to($correos[$i]);
			$this->email->subject('Recupera tu contraseña en Casa Hogar Vallado A.C.');
			$mensaje = $this->load->view('inicio/confirmar',"",TRUE);
			$this->email->message($mensaje);
			$this->email->send();
		}
		
		
	}
}

/* End of file  */
/* Location: ./application/controllers/ */

