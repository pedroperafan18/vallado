<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Controller Controller
 * --------------------------------------
 * Author       : $Author$
 * Revision     : $Revision$
 * Date         : $Date$
 * Position     : $HeadURL$
 *
 */

class Panel extends CI_Controller {
  
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('encrypt');
		
		//$this->load->model('mpanel');
		//Construct Code...
	}

	public function index()
	{
		if($this->session->userdata('admon')){
			redirect("panel/dashboard");	
		}
		
		if($this->input->get()!=NULL ||$this->input->post()!=NULL){
			$datos = ($this->input->get()!=NULL)? $this->input->get(): $this->input->post();
			$this->form_validation->set_rules('Usuario', 'Usuario', 'trim|required');
			$this->form_validation->set_rules('Password', 'Contraseña', 'trim|required');
			if ($this->form_validation->run() == FALSE) {
				$data['titulo'] = "Casa Hogar Vallado A.C.";
				$data['css'] = array('bootstrap.min');
				$data['script'] = array('jquery', 'bootstrap');
				$data['menu'] = $this->load->view('menu', "", TRUE);
				$data['contenido'] = $this->load->view('panel/login', "", TRUE);
				$this->load->view('html', $data, FALSE);
			}else {
				if($datos["Usuario"]=="Administrador"&&$datos["Password"]=="Vallado2015#"){
					$this->session->set_userdata( array('admon' => $this->encrypt->encode("vallado2015")) );
					$data['titulo'] = "Casa Hogar Vallado A.C.";
					$data['css'] = array('bootstrap.min');
					$data['script'] = array('jquery', 'bootstrap');
					$data['menu'] = $this->load->view('menu', "", TRUE);
					$data['contenido'] = $this->load->view('panel/index', "", TRUE);
					$this->load->view('html', $data, FALSE);
				}else{
					$datos["error"] = "Usuario y/o contraseña incorrectos";
					$data['titulo'] = "Casa Hogar Vallado A.C.";
					$data['css'] = array('bootstrap.min');
					$data['script'] = array('jquery', 'bootstrap');
					$data['menu'] = $this->load->view('menu', "", TRUE);
					$data['contenido'] = $this->load->view('panel/login', $datos, TRUE);
					$this->load->view('html', $data, FALSE);
				}
			}
		}else{
			$data['titulo'] = "Casa Hogar Vallado A.C.";
			$data['css'] = array('bootstrap.min');
			$data['script'] = array('jquery', 'bootstrap');
			$data['menu'] = $this->load->view('menu', "", TRUE);
			$data['contenido'] = $this->load->view('panel/login', "", TRUE);
			$this->load->view('html', $data, FALSE);	
		}
	}
	public function dashboard()
	{
		if(!$this->session->userdata('admon')){
			redirect("panel/index");	
		}
			
		$data['titulo'] = "Casa Hogar Vallado A.C.";
		$data['css'] = array('bootstrap.min');
		$data['script'] = array('jquery', 'bootstrap');
		$data['menu'] = $this->load->view('menu', "", TRUE);
		$data['contenido'] = $this->load->view('panel/index', "", TRUE);
		$this->load->view('html', $data, FALSE);
	}
}