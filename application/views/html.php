<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title><?=$titulo?></title>
	<?php
      foreach ($css as $key => $value) {
        echo '<link href="'.base_url().'css/'.$value.'.css" rel="stylesheet">'."\n";
      }
    ?>
</head>
<body>
	<?php
	if(isset($extras)){
		echo $extras;
	}
	?>
	
	<?=$menu?>
	<div class="container">
		<?=$contenido?>
	</div>
	<script>
		base_url = "<?php echo base_url()?>";
	</script>
	<?php
		foreach ($script as $key => $value) {
			echo '<script src="'.base_url().'js/'.$value.'.js"></script>'."\n";
		}
	?>
</body>
</html>