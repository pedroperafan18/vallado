<div class="row-fluid">
	<div class="col-md-6 col-md-offset-3">
		<div class="well text-center">
			<h2>¡Bienvenido!</h2>
			<h3>Posada con amig@s de Vallado 2015.</h3>
			<a href="<?php echo base_url("registro");?>" class="btn btn-primary">Registrarse</a>
			ó
			<a href="<?php echo base_url("login");?>" class="btn btn-success">Iniciar Sesión</a>
			<br>	
			<small class="text-muted text-justify">
				Amig@s la fecha límite para registro como padrino o donador es el viernes 27 de Noviembre a las 12:00 p.m. <br/>
				Los datos que nos proporcione quedan bajo estricta confidencialidad.
			</small>
		</div>
	</div>
</div>