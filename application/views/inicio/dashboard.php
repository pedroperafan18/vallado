<div class="row-fluid">
	<div class="col-md-4">
		<ul class="nav nav-pills nav-stacked">
		  <li role="presentation" class="active"><a href="<?php echo base_url("encuesta");?>">Encuesta</a></li>
		  <li role="presentation"><a href="<?php echo base_url("posada");?>">Posada</a></li>
		  <li role="presentation"><a href="<?php echo base_url("mapa");?>">Mapa</a></li>
		  <li role="presentation"></li>
		  <li role="presentation">
		  	Soporte técnico: <br>
		  	<b>Pedro Perafán  </b><br>
		  	pedroperafan18@gmail.com <br>
		  	(444) 1-14-41-73 <br>
		  	<b>Luis Gerardo Enriquez </b><br>
		  	lge_valdez@hotmail.com <br>
			(444) 3-33-40-88
		  </li>
		</ul>
	</div>
	<div class="col-md-8">
		<div class="well text-center">
			<h2>¡Bienvenido!</h2>
			<h3>Posada con amig@s de Vallado 2015.</h3>

			<img src="<?php echo base_url("img/logo.png");?>" alt="" class="img-responsive" style="margin:0 auto">
			<h4>¡Está es la 5° posada con amig@s de Vallado A.C.! </h4>
			<h5>Amig@s la fecha límite para registro como padrino o donador es el viernes 27 de Noviembre a las 12:00 p.m.</h5>
		</div>
	</div>
</div>