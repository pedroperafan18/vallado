<div class="row-fluid">
  <div class="col-md-6 col-md-offset-3 well">
  <h4 class="text-center">
    Posada con amig@s de Vallado 2015.
  </h4>
    <div class="row">
      <div class="col-md-4 col-md-offset-4 text-center">
        <img class="img-responsive" src="<?php echo base_url("img/logo.png");?>"/>
      </div>
    </div>
    <?php echo validation_errors(); ?>
    <?php if(isset($error)){ echo $error;}?>
    <form class="form-horizontal" method="post">
      <div class="form-group">
        <label for="inputPassword3" class="col-sm-4 control-label">Nueva contraseña</label>
        <div class="col-sm-8">
          <input type="password" class="form-control" name="Password">
        </div>
      </div>
      <input type="hidden" name="token" <?php echo "value='$token'";?>>
      <div class="form-group">
        <div class="col-sm-offset-4 col-sm-8">
          <button type="submit" class="btn btn-primary">Cambiar contraseña</button>
        </div>
      </div>
    </form>
      <small class="text-muted">
        Los datos que nos proporcione quedan bajo estricta confidencialidad.
      </small>
  </div>
</div>