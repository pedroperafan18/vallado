<div class="row-fluid">
	<div class="col-md-10 col-md-offset-2 well">
		<h3 class="text-center">Posada con amig@s de Vallado 2015.</h3>
		<img src="<?php echo base_url("img/logo.png");?>" alt="" class="img-responsive" style="margin:0 auto">
		<h3 class="text-center">FECHA DEL EVENTO.</h3>
		<p class="text-center">
			Te recordamos que el evento será el sábado 12 de Diciembre de las 11:00 de la mañana a 6:00 de la tarde en restaurante “Chantico” (A un lado del restaurante J.L.). Abajo se encuentra el mapa para llegar al lugar.
		</p>
		<img src="<?php echo base_url("img/mapa.png");?>" alt="" class="img-responsive" style="margin:0 auto">
		<p class="text-center">
			¿Quieres viajar con nosotros a Chantico?  Te ofrece transporte de ida y vuelta. Saldrá un camión de la casa hogar Vallado A.C., a las 10:15 de la mañana  del sábado 12 de diciembre en calle Maya #130 (a un costado del panteón de Valle de los Cedros).
		</p>
	</div>
</div>