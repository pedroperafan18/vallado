<div class="row-fluid">
	<div class="col-md-10 col-md-offset-1 well">
		<h1>¡Error! Este token ya no es válido.</h1>
		Vuelve a dar click en <a href="<?php echo base_url("recuperar");?>">recuperar contraseña.</a>
	</div>
</div>