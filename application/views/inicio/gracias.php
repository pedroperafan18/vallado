<div class="row-fluid">
	<div class="col-md-8 col-md-offset-2 well">
		<button class="btn btn-small">
			<span id="silencio" class="glyphicon glyphicon-volume-off"></span>
		</button>
		<audio id="audio" src="<?php echo base_url("img/cancion.mp3")?>" autoplay loop></audio>
		<h3 class="text-center">Posada con amig@s de Vallado 2015.</h3> 
		<img src="<?php echo base_url("img/logo.png");?>" alt="" class="img-responsive" style="margin:0 auto">
		<h2 class="text-center">¡ G r a c i a s !</h2>

		<img src="<?php echo base_url("img/amigos.png");?>" alt="" class="img-responsive" style="margin:0 auto">
	</div>
	<div class="col-md-8 col-md-offset-2" style="margin-bottom:10px">
		<a href="<?php echo base_url("inicio");?>" class="btn btn-primary">Continuar</a>
	</div>
</div>