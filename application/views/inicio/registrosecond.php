 <div class="row-fluid">
  <div class="col-md-6 col-md-offset-3 well">
  <?php echo validation_errors(); ?>
    <?php if(isset($error)){ echo $error;}?>
    <form class="form-horizontal" method="post">
       <div class="form-group">
              <label for="inputPassword3" class="col-sm-4 control-label">Edad</label>
              <div class="col-sm-8">
                <select name="Edad" id="" class="form-control">
                  <option value="1">18 a 25 años</option>
                  <option value="2">26 a 30 años</option>
                  <option value="3">31 a 35 años</option>
                  <option value="4">36 a 40 años</option>
                  <option value="5">41 a 45 años</option>
                  <option value="6">46 ó más años</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-4 control-label">Género</label>
              <div class="col-sm-8">
                <select name="Genero" id="" class="form-control">
                  <option value="1">Masculino</option>
                  <option value="2">Femenino</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-4 control-label">Ocupación</label>
              <div class="col-sm-8">
                <select name="Ocupacion" id="Ocupacion" class="form-control">
                  <option value="1">Estuadiante</option>
                  <option value="2">Ama de casa</option>
                  <option value="3">Profesionista</option>
                  <option value="4">Empleado</option>
                  <option value="5">Otro</option>
                </select>
              </div>
            </div>
            <div id="OcupacionText" class="form-group hidden">
              <div class="col-md-offset-4 col-sm-8">
                 <input type="text" class="form-control" name="Ocupacion1">
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-4 control-label">Estado Civil</label>
              <div class="col-sm-8">
                <select name="EstadoCivil" id="EstadoCivil" class="form-control">
                  <option value="1">Soltero/a</option>
                  <option value="2">Casado/a</option>
                  <option value="3">Viudo/a</option>
                  <option value="4">Otro</option>
                </select>
              </div>
            </div>
            <div id="EstadoCivilText" class="form-group hidden">
              <div class="col-md-offset-4 col-sm-8">
                 <input type="text" class="form-control" name="EstadoCivil1">
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-4 control-label">Nivel de estudios</label>
              <div class="col-sm-8">
                <select name="Estudios" id="Estudios" class="form-control">
                  <option value="1">Secundaria</option>
                  <option value="2">Bachillerato</option>
                  <option value="3">Licenciatura</option>
                  <option value="4">Otros</option>
                </select>
              </div>
            </div>
            <div id="EstudiosText" class="form-group hidden">
              <div class="col-md-offset-4 col-sm-8">
                 <input type="text" class="form-control" name="Estudios1">
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-4 control-label">Número total de integrantes en su hogar</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" name="Hogar">
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-4 control-label">Pasatimpo recurrente</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" name="Pasatiempo">
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-4 control-label">¿Cómo le gustaría ser contactado para recibir información de Vallado?</label>
              <div class="col-sm-8">
                <select name="Contacto" id="Contacto" class="form-control">
                  <option value="1">Correo electrónico</option>
                  <option value="2">Teléfono celular, casa o trabajo.</option>
                  <option value="3">Aplicación móvil.</option>
                  <option value="4">Redes sociales.</option>
                  <option value="5">Otro.</option>
                </select>
              </div>
            </div>
            <div id="ContactoText" class="form-group hidden">
              <div class="col-md-offset-4 col-sm-8">
                 <input type="text" class="form-control" name="Contacto1">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" class="btn btn-primary">Regístrate</button>
              </div>
            </div>
      </form>
    </div>
  </div>