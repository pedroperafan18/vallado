<div class="row-fluid">
	<div class="col-md-12">
		<h3>A continuación viene la lista de amig@s de Vallado A.C., y las necesidades que es nuestra metra cumplir para la celebración. </h3>
		<h2 class="text-center">¡Ahora podrás seleccionar a tu amig@ de Vallado A.C.!</h2>
		<?php
		if($asistentes!=FALSE){
			echo '<div class="col-md-3"><a href="'.base_url("gracias").'" class="btn btn-primary btn-block">¡Ver mensaje de gracias!</a><br/></div>';
		}
		?>
		<table class="table table-bordered">
			<tr>
				<th>Nombre</th>
				<th>Edad</th>
				<th>Talla Ropa</th>
				<th>Talla Calzado</th>
				<th>Regalo Navidad</th>
				<th>Regalo de Invierno. (Ropa)</th>
				<th>Regalo de Invierno. (Calzado)</th>
				<th>Regalo Navidad</th>
			</tr>
			<?php
			foreach ($ninos as $key => $value) {
				for ($i=1; $i < 4; $i++) { 
					if(isset($listaNinos[($key+1).'-'.$i])){
						$o[$i] = $listaNinos[($key+1).'-'.$i];
					}else{
						$o[$i] = '<input type="checkbox" class="form-control ninos" data-opcion="'.($key+1).'-'.$i.'">';
					}
				}
				
				echo '<tr>
						<td>'.$value["Nombre"].'</td>
						<td>'.$value["Edad"].'</td>
						<td>'.$value["Talla_ropa"].'</td>
						<td>'.$value["Talla_calzado"].'</td>
						<td>'.$value["Regalo"].'</td>
						<td>'.$o["1"].'</td>
						<td>'.$o["2"].'</td>
						<td>'.$o["3"].'</td>
					</tr>';
			}
			?>
		</table>

		<table class="table table-bordered">
			<tr>
				<th></th>
				<th></th>
				<th>¡Yo pongo todo!</th>
				<th>Elegir cantidad</th>
				<th>Padrinos</th>
			</tr>
			<?php
			foreach ($comida as $key => $value) {
				if($value["Tipo"]==1){
					$faltan =  '(Faltan $'.$listaComida[($key+1)]["Falta"].')';
				}else{
					$faltan =  '(Faltan '.$listaComida[($key+1)]["Falta"].')';
				}
				$todo = (isset($listaComida[($key+1)]["Todo"]))? '':'<input type="checkbox" class="form-control comida" data-opcion="'.($key+1).'">';
				$aportar = ($listaComida[($key+1)]["Falta"]==0)? '' : '<input type="number" class="form-control comida comidaText comidaText'.($key+1).'" data-opcion="'.($key+1).'" data-limite="'.$listaComida[($key+1)]["Falta"].'">';
				$padrinos = (isset($listaComida[($key+1)]["Padrinos"]))? $listaComida[($key+1)]["Padrinos"]: '';
				echo '<tr>
						<td>'.$value["Item"].'</td>
						<td>'.$value["Descripcion"].' <b>'.$faltan.'</b></td>
						<td>'.$todo.'</td>
						<td>'.$aportar.'</td>
						<td>'.$padrinos.'</td>
					</tr>';
			}
			?>
		</table>
		<div class="col-md-8 col-md-offset-2">
			<div class="form-group">
				<button class="btn btn-success btn-lg btn-block temp">Confirmar</button>
			</div>
		</div>
	</div>
	
</div>