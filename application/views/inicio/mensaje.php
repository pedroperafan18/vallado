<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Gracias por registrarte a Casa Hogar Vallado A.C</title>
    <link href="<?php echo base_url('css/bootstrap.min.css');?>" rel="stylesheet">
  </head>
<body>
	<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 well well-sm">
		<h2>¡Hola <?php echo $nombre;?>! </h2>
		Gracias por registrarte en el sitio de Casa Hogar Vallado A.C. <br>
		<br>
		Primero es necesario contestar una encuesta : <a href="<?php echo base_url("encuesta");?>" target="_blank">Encuesta</a>  <br>
		Puedes hacer tus aportaciones ingresando a : <a href="<?php echo base_url("posada");?>"  target="_blank">Posada</a>
	</div>
</body>
</html>