<div class="col-md-8 col-md-offset-2 well">
	<form class="form-horizontal">
		<div class="radio">
			<label>
				<input type="radio" name="asistencia" class="asistencia" value="si" checked>
				Si
			</label>
		</div>
		<?php
		for ($i=1; $i <= 5; $i++) { 
		?>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-3 control-label">Asistente <?php echo $i;?></label>
			<div class="col-sm-4">
				<input type="number" class="form-control asistentes asistentes-<?php echo $i;?> asistente-edad" data-asistente="<?php echo $i;?>" value="<?php echo $asistentes["Edad".$i];?>" placeholder="Edad">
			</div>
			<div class="col-sm-5">
				<input type="number" class="form-control asistentes asistentes-<?php echo $i;?> asistente-talla" data-asistente="<?php echo $i;?>"  value="<?php echo $asistentes["Talla".$i];?>" placeholder="Talla">
			</div>
		</div>
		<?php
		}
		?>
		<div class="radio">
			<label>
				<input type="radio" name="asistencia" class="asistencia" value="no">
				No (Favor de mandar un correo electrónico a vallado_posada2015@outlook.com para indicar la forma de hacer llegar tú donativo).  
			</label>
		</div>
		<div class="enviar btn btn-primary">Enviar</div>
	</form>

</div>