<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Casa Hogar Vallado A.C</title>
    <link href="<?php echo base_url('css/bootstrap.min.css');?>" rel="stylesheet">
  </head>
<body>
	<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 well well-sm">
		<h2>¡Hola! </h2>
		Gracias por apoyarnos en Casa Hogar Vallado A.C.<br>
		<br>
		Necesitamos que confirmes tu asistencia : <a href="<?php echo base_url("inicio/confirmarAsistencia");?>" target="_blank">Confirmar Asistencia</a>  <br>
		<b>Te pedimos que si ya habías hecho este proceso lo vuelvas a realizar para corroborar nuestra información.</b>
	</div>
</body>
</html>