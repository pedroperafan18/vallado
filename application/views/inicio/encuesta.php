<div class="row-fluid">
	<div class="col-md-8 col-md-offset-2 well">
		<div class="pregunta">
			<h3 class="text-center">ACERCA DE TÍ COMO DONADOR DE VALLADO A.C.</h3>
			<img src="<?php echo base_url("img/logo.png");?>" alt="" class="img-responsive" style="margin:0 auto">
		</div>
		<div class="respuestas">
			
		</div>
		<div class="form-group" id="otros" style="display:none">
			<div class="col-sm-12">
				<input type="text" class="form-control" id="otro" />
			</div>
		</div>
		<button class="btn btn-primary siguiente" style="display:none">Siguiente</button>
		<button class="btn btn-primary listo" style="display:none">Finalizar</button>
		<button class="btn btn-primary iniciar">Iniciar</button> (<span id="numero">0 de 12</span>)
	</div>
	<div class="result">
		
	</div>
</div>