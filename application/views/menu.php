<nav class="navbar navbar-inverse navbar-static-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url("inicio");?>">CASA HOGAR VALLADO A.C.</a>
    </div>
    <?php 
    if($this->session->userdata('usuario')){
    ?>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="<?php echo base_url("inicio/salir");?>">Cerrar Sesión</a></li>
    </ul>
    <?php 
    }
    ?>
  </div><!-- /.container-fluid -->
</nav>