-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-11-2015 a las 08:43:19
-- Versión del servidor: 10.0.17-MariaDB
-- Versión de PHP: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vallado`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistencia`
--

CREATE TABLE `asistencia` (
  `ID` int(5) NOT NULL,
  `ID_Donador` int(5) NOT NULL,
  `Edad1` int(2) NOT NULL,
  `Edad2` int(2) NOT NULL,
  `Edad3` int(2) NOT NULL,
  `Edad4` int(2) NOT NULL,
  `Edad5` int(2) NOT NULL,
  `Talla1` int(2) NOT NULL,
  `Talla2` int(2) NOT NULL,
  `Talla3` int(2) NOT NULL,
  `Talla4` int(2) NOT NULL,
  `Talla5` int(2) NOT NULL,
  `Fecha` datetime NOT NULL,
  `Habilitado` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comida`
--

CREATE TABLE `comida` (
  `ID` int(2) NOT NULL,
  `Item` varchar(100) NOT NULL,
  `Descripcion` varchar(400) NOT NULL,
  `Costo` int(10) NOT NULL,
  `Tipo` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `comida`
--

INSERT INTO `comida` (`ID`, `Item`, `Descripcion`, `Costo`, `Tipo`) VALUES
(1, 'Tamales.', '400 ', 3200, 1),
(2, 'Atole.', '25 litros  ', 750, 1),
(3, 'Pastel.', '100 personas. ', 1500, 1),
(4, 'Refrescos.', '30 (2 lts). ', 30, 2),
(5, 'vasos térmicos.', '100\r\n', 100, 2),
(6, 'vasos de plástico. ', '100', 100, 2),
(7, 'platos tipo charola. ', '150', 150, 2),
(8, 'platos pastelero', '150', 150, 2),
(9, 'cucharas', '150', 150, 2),
(10, 'tenedores', '150', 150, 2),
(11, 'servilletas', '200', 200, 2),
(12, 'bolsas negras jumbo.', '10', 10, 2),
(13, 'Frituras y/o botana. ', '50 (1kg por bolsa). ', 50, 2),
(14, 'Salsa.', '3 botellas (1 litro).', 3, 2),
(15, 'Piñatas', '2', 2, 2),
(16, 'Camión. ', '1 traslado para amigos Vallado ida y vuelta. ', 1450, 1),
(17, 'Bolsas de dulces. ', '80 bolsas armadas. ', 80, 2),
(18, 'Globos con helio.', '20 globos. (Con carta de amigos Vallado) ', 20, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_donadores`
--

CREATE TABLE `datos_donadores` (
  `ID_Donador` int(10) NOT NULL,
  `Edad` int(1) NOT NULL,
  `Genero` int(1) NOT NULL,
  `Ocupacion` varchar(100) NOT NULL,
  `EstadoCivil` varchar(100) NOT NULL,
  `Estudios` varchar(100) NOT NULL,
  `Hogar` int(11) NOT NULL,
  `Pasatiempo` varchar(150) NOT NULL,
  `Contacto` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `donacion_comida`
--

CREATE TABLE `donacion_comida` (
  `ID_Donacion` int(5) NOT NULL,
  `ID_Donador` int(5) NOT NULL,
  `Item` int(5) NOT NULL,
  `Cantidad` int(10) NOT NULL,
  `Fecha` datetime NOT NULL,
  `Habilitado` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `donacion_nino`
--

CREATE TABLE `donacion_nino` (
  `ID_Donacion` int(5) NOT NULL,
  `ID_Donador` int(5) NOT NULL,
  `ID_Nino` int(5) NOT NULL,
  `Regalo` int(1) NOT NULL,
  `Fecha` datetime NOT NULL,
  `Habilitado` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `donadores`
--

CREATE TABLE `donadores` (
  `ID` int(10) NOT NULL,
  `Nombre` varchar(200) NOT NULL,
  `ApellidoP` varchar(200) NOT NULL,
  `ApellidoM` varchar(200) NOT NULL,
  `Correo` varchar(100) NOT NULL,
  `Password` varchar(200) NOT NULL,
  `FechaRegistro` datetime NOT NULL,
  `FechaUltimoEntrada` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ninos`
--

CREATE TABLE `ninos` (
  `ID` int(10) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `Edad` int(2) NOT NULL,
  `Talla_ropa` varchar(100) NOT NULL,
  `Talla_calzado` int(2) NOT NULL,
  `Regalo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ninos`
--

INSERT INTO `ninos` (`ID`, `Nombre`, `Edad`, `Talla_ropa`, `Talla_calzado`, `Regalo`) VALUES
(1, 'Isamar Veilet. ', 6, '06 (niña). ', 17, 'Rompecabezas'),
(2, 'Obed. ', 9, '12 (niño). ', 22, 'Pista de carritos con carritos.'),
(3, 'David. ', 7, '08 (niño). ', 21, 'Rompecabezas.'),
(4, 'Ángel. ', 8, '10 (niño). ', 22, 'Balón de futbol.'),
(5, 'Emanuel. ', 9, '10 (niño). ', 22, 'Balón de futbol. '),
(6, 'Jazmín. ', 11, '12 (niña). ', 23, 'Perfume o  libros de leyendas o aventuras.'),
(7, 'Fátima. ', 13, 'T. Chica (Pantalón 14). ', 23, 'Perfume o  libros de leyendas o aventuras.'),
(8, 'Jaqueline. ', 15, 'T. Mediana. (Pantalón 14). ', 23, 'Perfume o  libros de leyendas o aventuras.'),
(9, 'Valeria. ', 6, '8 (niña). ', 20, 'Rompecabezas'),
(10, 'Lupita', 18, 'T. Grande. (Pantalón 32). ', 23, 'Chamarra o pantalón.'),
(11, 'Ángeles', 11, '12 (niña). ', 24, 'Perfume o  libros de leyendas o aventuras.'),
(12, 'María Ángela. ', 10, '12 (niña). ', 23, 'Juegos tipo Lince. (Para fijar atención)'),
(13, 'Marisol. ', 18, 'T. Grande. (Pantalón 36). ', 25, 'Chamarra o pantalón.'),
(14, 'Mary. (Encargada de menores). ', 27, 'T. Mediana. (Pantalón 28). ', 24, 'Chamarra o pantalón. '),
(15, 'Ana Lilia. (Encargada de menores).', 34, 'T. Mediana, (Pantalón 32). ', 24, 'Chamarra o pantalón'),
(16, 'Gaby (Encargada de menores).', 30, 'T. Mediana. (Pantalón 30). ', 24, 'Chamarra o pantalón.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas`
--

CREATE TABLE `preguntas` (
  `ID` int(5) NOT NULL,
  `Pregunta` varchar(400) NOT NULL,
  `R1` varchar(300) NOT NULL,
  `R2` varchar(300) NOT NULL,
  `R3` varchar(300) NOT NULL,
  `R4` varchar(300) NOT NULL,
  `R5` varchar(300) NOT NULL,
  `R6` varchar(300) NOT NULL,
  `Tipo` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `preguntas`
--

INSERT INTO `preguntas` (`ID`, `Pregunta`, `R1`, `R2`, `R3`, `R4`, `R5`, `R6`, `Tipo`) VALUES
(1, 'Por favor; elija las dos opciones según considere son las más importantes de acuerdo a las actividades que realiza la casa hogar Vallado A.C. ', 'Admiración por el trabajo de 13 años de Vallado.', 'Compasión  por la incertidumbre del futuro de los menores.', 'Empatía e identificación por la loable labor de Vallado.', 'Satisfacción por retribuir a la sociedad con mi apoyo.', 'Según mis dogmas religiosos. ', 'Otros.', 2),
(2, '¿Estaría interesado en ser parte de una red formal de donadores para Vallado A.C.?', 'Si.', 'No. ¿Por qué? ', '', '', '', '', 1),
(3, 'Si fuera parte de una red formal de donadores para Vallado A.C., estoy en posibilidades de hacer un donativo que regularmente sea...', 'Económico.', 'En especie (ropa, alimentos, medicina, etc.) ', 'Tiempo.', 'Otro ¿Cuál?', '', '', 1),
(4, 'Si mi donativo fuera económico, puede ser dentro del siguiente rango:', 'Menos de $100 pesos.', '$101-300 pesos.', '$301-500 pesos.', '$501 o más.', 'Otro:', '', 1),
(5, 'Para mí la mejor vía para donar es:', 'Tarjeta de crédito.', 'Cheque a nombre de la fundación.', 'Depósito bancario.', 'En efectivo.', 'Otro:', '', 1),
(6, 'Si fuera parte de una red formal de donadores de Vallado A.C., mi donativo económica podría ser:', 'Semanal.', 'Quincenal.', 'Mensual.', 'En actividades o festejos especiales.', 'Otro:', '', 1),
(7, 'Mi donativo en especie mayormente podría ser de...', 'Alimentos y líquidos  perecederos y no perecederos', 'Medicinas y productos de cuidado personal.', 'Ropa de vestir o interior y calzado (nuevo o usado en buenas condiciones).', 'Muebles, utensilios, herramientas.', 'Otros', '', 1),
(8, 'Cuando dono en especie, prefiero hacerlo llegar:', 'Personalmente.', 'Por medio de terceros.', 'Anónimamente.', 'Por medio de colectas.', 'Otro:', '', 1),
(9, 'Si fuera parte de una red formal de donadores de Vallado A.C., mi donativo en especia podría ser:', 'Semanal.', 'Quincenal.', 'Mensual.', 'En actividades o festejos especiales.', 'Otro.', '', 1),
(10, 'Puedo apoyar a Vallado A.C., con mi tiempo realizando las siguientes actividades (puede marcar el número de opciones que usted considere, no hay límite)', 'Visitas para estar al cuidado de los menos en las casa de Vallado A.C.', 'Ayudar con el mantenimiento y cuidado de la casa (Limpieza, reparación, etc.)', 'Tutorías escolares a los niños. (Matemáticas, Inglés, etc).', 'Enseñanza de actividades artísticas (Pintura, canto, teatro, danza).', 'Participar en la organización de eventos para recaudación de fondos. (Festejos, cumpleaños, etc.)', 'Otro.', 2),
(11, 'Aproximadamente cuantas horas a la semana estarías dispuesto a dedicarle a los niños', '3 horas', '4 horas', '5 horas', 'Otro', '', '', 1),
(12, '<h3 class="text-center">Gracias por su tiempo; estamos convencido de su interés genuino por nuestros amig@s de Vallado A.C.</h3>\r\n\r\n\r\nPara Vallado A.C su opinión es muy importante, cualquier observación y/o sugerencia puedes hacerla en esta área; agradecemos su tiempo y colaboración.\r\n', '', '', '', '', '', '', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas`
--

CREATE TABLE `respuestas` (
  `ID` int(11) NOT NULL,
  `A1` varchar(100) NOT NULL,
  `A1-1` varchar(100) NOT NULL,
  `A2` varchar(100) NOT NULL,
  `A3` varchar(100) NOT NULL,
  `A4` varchar(100) NOT NULL,
  `A5` varchar(100) NOT NULL,
  `A6` varchar(100) NOT NULL,
  `A7` varchar(150) NOT NULL,
  `A8` varchar(150) NOT NULL,
  `A9` varchar(150) NOT NULL,
  `A10` varchar(150) NOT NULL,
  `A10-1` varchar(150) NOT NULL,
  `A11` varchar(150) NOT NULL,
  `A12` varchar(150) NOT NULL,
  `Fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tokens`
--

CREATE TABLE `tokens` (
  `ID_Token` int(5) NOT NULL,
  `ID_Donador` int(5) NOT NULL,
  `Token` varchar(100) NOT NULL,
  `Fecha` datetime NOT NULL,
  `Habilitado` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `asistencia`
--
ALTER TABLE `asistencia`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indices de la tabla `comida`
--
ALTER TABLE `comida`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `datos_donadores`
--
ALTER TABLE `datos_donadores`
  ADD PRIMARY KEY (`ID_Donador`);

--
-- Indices de la tabla `donacion_comida`
--
ALTER TABLE `donacion_comida`
  ADD PRIMARY KEY (`ID_Donacion`);

--
-- Indices de la tabla `donacion_nino`
--
ALTER TABLE `donacion_nino`
  ADD PRIMARY KEY (`ID_Donacion`);

--
-- Indices de la tabla `donadores`
--
ALTER TABLE `donadores`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `ninos`
--
ALTER TABLE `ninos`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `preguntas`
--
ALTER TABLE `preguntas`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `respuestas`
--
ALTER TABLE `respuestas`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`ID_Token`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `asistencia`
--
ALTER TABLE `asistencia`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `comida`
--
ALTER TABLE `comida`
  MODIFY `ID` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `donacion_comida`
--
ALTER TABLE `donacion_comida`
  MODIFY `ID_Donacion` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `donacion_nino`
--
ALTER TABLE `donacion_nino`
  MODIFY `ID_Donacion` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `donadores`
--
ALTER TABLE `donadores`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `ninos`
--
ALTER TABLE `ninos`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `preguntas`
--
ALTER TABLE `preguntas`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `tokens`
--
ALTER TABLE `tokens`
  MODIFY `ID_Token` int(5) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
